#!/usr/bin/env bash
############################
#owner: droritzz
#purpose: this part of the script will create, manage and config user for the new server. 
#date: 03.02.21
#version: in git commit
###########################

########### all the actions require root ###########

########## DEBIAN DISTRIBUTIONS ################
validation(){
    if [[ $? == 0 ]]; then
    echo "done"
else
    echo "wasn't able to complete the task."
    exit
fi
}

users_debian(){}
echo "creating server admin user: username, password, adding to groups, creating aliases."
read -p "please enter user name for the server admin: " username
useradd -m -G sudo,adm -s /bin/bash $username
read -s -p "please enter password for the $username : " password 
echo -e "$password\n$password" | passwd $username

cat /etc/passwd | grep $username

validation

echo "adding new aliases for the server."

echo "############### CUSTOM ALIASES ##############
alias vi=vim
alias l=ls
alias ll='ls -la'
alias cl=clear
alias cp='cp -v'
alias please=sudo
alias rm='rm -i'
" >> /etc/bash.bashrc

validation

echo "installing  vim."
apt install -y vim

validation
}

users_debian



